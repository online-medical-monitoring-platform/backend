package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Caregiver;

public class ActivityBuilder {
    private ActivityBuilder() {

    }

    public static ActivityDTO toActivityDTO(Activity activity) {
        return new ActivityDTO(activity.getId(),activity.getPatientId(),activity.getActivity(), activity.getStart(), activity.getEnd());
    }

    public static Activity toEntity(ActivityDTO activityDTO){
        if (activityDTO.getId()==null)
            return new Activity(
                    activityDTO.getPatientId(),
                    activityDTO.getActivity(),
                    activityDTO.getStart(),
                    activityDTO.getEnd()
            );
        else
            return new Activity( activityDTO.getId(),
                    activityDTO.getPatientId(),
                    activityDTO.getActivity(),
                    activityDTO.getStart(),
                    activityDTO.getEnd()
            );
    }
}
