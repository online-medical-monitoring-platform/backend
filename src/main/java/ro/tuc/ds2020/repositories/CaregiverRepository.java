package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.Optional;

public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Caregiver> findByName(String name);
    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT c " +
            "FROM Caregiver c " +
            "WHERE c.name = :name "+
            "and (:patients is null"+
            " or c.patients = :patients)")
    List<Caregiver> findAllByName(@Param("name") String name, @Param("patients") List<Patient> patients);

    @Query(value = "select p from Patient p " +
            "join Caregiver c on p.caregiver = c " +
            "where c.id=:id")
    List<Patient> getPatients(@Param("id") Long id);

}
