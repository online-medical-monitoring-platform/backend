package ro.tuc.ds2020.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.UserApp;
import ro.tuc.ds2020.dtos.UserAppDTO;
import ro.tuc.ds2020.services.UserAppService;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserAppController {

    private final UserAppService userService;

    @Autowired
    public UserAppController(UserAppService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UserAppDTO>> getUsers() {
        List<UserAppDTO> dtos = userService.findUsers();
        for (UserAppDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatient(dto.getId())).withRel("patientDetails");
            dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertUser(@Valid @RequestBody UserAppDTO patientDTO) {
        Long patientID = userService.insert(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserAppDTO> getUser(@PathVariable("id") Long patientId) {
        UserAppDTO dto = userService.findUserAppById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/{username}/{password}")
    public ResponseEntity<UserAppDTO> getPatientByUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        UserAppDTO user = userService.findUserByUsernameAndPassword(username, password);
        System.out.println("Username si parola :"+username+ "  "+ password);
        System.out.println("Pacient gasit"+ user);
        return new ResponseEntity<>(user, HttpStatus.OK);

    }



    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deleteUser(@PathVariable("id") Long id){
        Long personID = userService.delete(id);
        return new ResponseEntity<>(personID, HttpStatus.OK);
    }

    /*  @DeleteMapping(value = "/{name}")
      public ResponseEntity<Long> deletePatientByName(@PathVariable("name") String name){
          Long patientID = userService.deleteByName(name);
          return new ResponseEntity<>(patientID, HttpStatus.OK);
      }
  */
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody UserAppDTO patientUpdateDTO) {
        Long patientID = userService.update(id, patientUpdateDTO);
        return new ResponseEntity<>(patientID, HttpStatus.ACCEPTED);
    }
}

