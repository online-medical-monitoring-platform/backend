package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.entities.MedicationPrescription;

public class MedicationPrescriptionBuilder {

    private MedicationPrescriptionBuilder() {
    }

    public static MedicationPrescriptionDTO toMedicationPrescriptionDTO(MedicationPrescription medicationPrescription) {
        if(medicationPrescription.getAdministrated()==null)
        return new MedicationPrescriptionDTO(medicationPrescription.getId(), medicationPrescription.getIntake(), MedicationBuilder.toMedicationDTO(medicationPrescription.getMedication()),MedicationPlanBuilder.toMedicationPlanDTO(medicationPrescription.getMedicationPlan()));
        else
            return new MedicationPrescriptionDTO(medicationPrescription.getId(), medicationPrescription.getIntake(), MedicationBuilder.toMedicationDTO(medicationPrescription.getMedication()),MedicationPlanBuilder.toMedicationPlanDTO(medicationPrescription.getMedicationPlan()), medicationPrescription.getAdministrated());

    }

    public static MedicationPrescription toEntity(MedicationPrescriptionDTO medicationPrescriptionDTO) {
        if(medicationPrescriptionDTO.getAdministrated()==null)
            return new MedicationPrescription( medicationPrescriptionDTO.getId(),
                    medicationPrescriptionDTO.getIntake(),
                    MedicationBuilder.toEntity(medicationPrescriptionDTO.getMedication()),
                    MedicationPlanBuilder.toEntity(medicationPrescriptionDTO.getMedicationPlan()));
        else
            return new MedicationPrescription( medicationPrescriptionDTO.getId(),
                    medicationPrescriptionDTO.getIntake(),
                    MedicationBuilder.toEntity(medicationPrescriptionDTO.getMedication()),
                    MedicationPlanBuilder.toEntity(medicationPrescriptionDTO.getMedicationPlan()),
                    medicationPrescriptionDTO.getAdministrated());
    }

}
