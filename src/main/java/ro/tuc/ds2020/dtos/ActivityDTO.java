package ro.tuc.ds2020.dtos;

import java.util.Objects;

public class ActivityDTO {

    private Long id;
    private Long patientId;
    private String activity;
    private String start;
    private String end;

    public ActivityDTO(){

    }

    public ActivityDTO(Long id, Long patientId, String activity, String start, String end) {
        this.id = id;
        this.patientId = patientId;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public Long getId() {
        return id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public String getActivity() {
        return activity;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityDTO activityDTO = (ActivityDTO) o;
        return  Objects.equals(patientId, activityDTO.patientId) &&
                Objects.equals(start, activityDTO.start) &&
                Objects.equals(end, activityDTO.end) &&
                Objects.equals(activity, activityDTO.activity) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientId, start, end,activity);
    }

    @Override
    public String toString() {
        return activity+" Patient id: "+patientId+" e de tip"+patientId.getClass();
    }
}
