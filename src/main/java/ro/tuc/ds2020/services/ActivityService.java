package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.repositories.ActivityRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.text.ParseException;


@Service
public class ActivityService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }




    public List<ActivityDTO> findActivities() {
        List<Activity> activites = activityRepository.findAll();
        return activites.stream()
                .map(ActivityBuilder::toActivityDTO)
                .collect(Collectors.toList());
    }

    public ActivityDTO findActivityById(Long id) {
        Optional<Activity> prosumerOptional = activityRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Activity with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return ActivityBuilder.toActivityDTO(prosumerOptional.get());
    }


    public Long insert(ActivityDTO activityDTO) {
        Activity activity = ActivityBuilder.toEntity(activityDTO);
        activity = activityRepository.save(activity);
        LOGGER.debug("Activity with id {} was inserted in db", activity.getId());
        return activity.getId();
    }

    public Long delete(Long id){
        Activity activity= activityRepository.findById(id).get();
        activityRepository.delete(activity);
        LOGGER.debug("ACTIVITY WITH ID {} DELETED", id);
        return id;
    }

    public long getDurationInMinutes(ActivityDTO activityDTO) {

        long hours = 0;
        long minutes=0;

        try {
            // yyyy not YYYY
            Date startDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityDTO.getStart());
            Date endDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityDTO.getEnd());

            hours = endDateFormat.getHours() - startDateFormat.getHours();
            if(hours<0){
                hours=24+hours;
            }
            minutes=endDateFormat.getMinutes()-startDateFormat.getMinutes();
            if(minutes<0){
                minutes=60+minutes;
                hours--;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hours*60+minutes;
    }


}

