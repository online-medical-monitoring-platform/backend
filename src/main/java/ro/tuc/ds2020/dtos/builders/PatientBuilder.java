package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        if(patient.getCaregiver()==null)
            return new PatientDTO(patient.getId(),patient.getUsername(), patient.getPassword(), patient.getName(), patient.getBirthdate(), patient.getGender(), patient.getAddress(),patient.getMedicalRecord());
        else
            return new PatientDTO(patient.getId(),patient.getUsername(), patient.getPassword(), patient.getName(), patient.getBirthdate(), patient.getGender(), patient.getAddress(),CaregiverBuilder.toCaregiverDTO(patient.getCaregiver()),patient.getMedicalRecord());

    }


    public static Patient toEntity(PatientDTO personDTO) {
            if(personDTO.getCaregiver()!=null)
                return new Patient( personDTO.getId(),
                        personDTO.getUsername(),
                        personDTO.getPassword(),
                        personDTO.getName(),
                        personDTO.getBirthdate(),
                        personDTO.getGender(),
                        personDTO.getAddress(),
                        CaregiverBuilder.toEntity(personDTO.getCaregiver()),
                        personDTO.getMedicalRecord());
            else
                return new Patient( personDTO.getId(),
                        personDTO.getUsername(),
                        personDTO.getPassword(),
                        personDTO.getName(),
                        personDTO.getBirthdate(),
                        personDTO.getGender(),
                        personDTO.getAddress(),
                        personDTO.getMedicalRecord());
    }

}
