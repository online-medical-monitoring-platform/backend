package ro.tuc.ds2020.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="patient")
//@PrimaryKeyJoinColumn(name = "id")
public class Patient extends UserApp{

  /*  @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;
*/
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "birthdate", nullable = true)
    private String birthdate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address" , nullable = true)
    private String address;


//    @JsonManagedReference
    @ManyToOne                   //(fetch = FetchType.LAZY)
    @JoinColumn(name="caregiver", nullable = true)//, insertable = false, updatable = false)
    private Caregiver caregiver;

    @Column(name="medical_record")
    private String medicalRecord;

    @OneToMany(cascade=CascadeType.REMOVE , mappedBy = "patient", fetch =FetchType.EAGER)//, orphanRemoval = true)
    private List<MedicationPlan> medicationplans;


    public Patient(Long id,String username,String password, String name, String birthdate, String gender, String address, Caregiver caregiver,String medicalRecord) {
        this.setId(id);
        this.setUsername(username);
        this.setPassword(password);
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.caregiver=caregiver;
        this.medicalRecord=medicalRecord;
        this.setRole("patient");
    }

    public Patient(Long id,String username,String password, String name, String birthdate, String gender, String address,String medicalRecord) {
        this.setId(id);
        this.setUsername(username);
        this.setPassword(password);
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord=medicalRecord;
        this.setRole("patient");
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }


    public Patient(){

    }


    @Override
    public String toString() {
        return "Patient [id=" + getId() + ", name=" + name + "]";
    }


}
