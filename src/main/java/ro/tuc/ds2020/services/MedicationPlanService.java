package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPrescriptionBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService{ //implements MedicationPlanServiceInterface{
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);


   /* @Autowired
    MedicationPlanRepository medicationPlanRepository;

    public MedicationPlanService(){}*/

    private final MedicationPlanRepository medicationPlanRepository;
    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }


    //@Override
    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll();
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    //@Override
    public MedicationPlanDTO findMedicationPlanById(Long id) {
        Optional<MedicationPlan> prosumerOptional = medicationPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication Plan with id {} was not found in db", id);
            //throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(prosumerOptional.get());
    }

    //@Override
    public List<MedicationPlanDTO> findMedicationPlanByPatient(PatientDTO patient) {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findByPatient(PatientBuilder.toEntity(patient));
        if (!(medicationPlans.size()>0)) {
            LOGGER.error("Medication Plan with patient id {} was not found in db", patient.getId());
            //throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
            MedicationPlanDTO medPlan= new MedicationPlanDTO((long) 0,"notFound","notFound","notFound",patient);
            List<MedicationPlanDTO> list= new ArrayList<MedicationPlanDTO>();
            list.add(medPlan);
            return list;
        }
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    //@Override
    public List<MedicationPrescriptionDTO> getMedicationPrescriptions(Long id) {
        List<MedicationPrescription> plans = medicationPlanRepository.getMedicationPrescriptions(id);
       /* System.out.println(a.size());
        System.out.println(a.get(0).toString());
        List<String> lista = Arrays.asList(a.get(0).toString().split(" "));
        System.out.println("lista este: " + lista.size());
        for(String s : lista){
            System.out.println(s);
        }*/
        System.out.println(plans.size());
        return plans.stream()
                .map(MedicationPrescriptionBuilder::toMedicationPrescriptionDTO)
                .collect(Collectors.toList());
    }

    //@Override
    public Long insert(MedicationPlanDTO medicationDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationDTO);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("medication Plan with id {} was inserted in db", medicationPlan.getId());
        return medicationPlan.getId();
    }

    //@Override
    public Long delete(Long id){
        MedicationPlan medication = medicationPlanRepository.findById(id).get();
        medicationPlanRepository.delete(medication);
        LOGGER.debug("MEDICATION Plan WITH ID {} DELETED", id);

        return id;
    }

    //@Override
    public Long update(Long id, MedicationPlanDTO medicationPlanUpdateDTO) {

        if (medicationPlanRepository.findById(id).isPresent()){
            MedicationPlan existingMedicationPlan = medicationPlanRepository.findById(id).get();
            if(medicationPlanUpdateDTO.getPatient()==null)
                medicationPlanUpdateDTO.setPatient(PatientBuilder.toPatientDTO(existingMedicationPlan.getPatient()));

            MedicationPlan updatesMedicationPlan= MedicationPlanBuilder.toEntity(medicationPlanUpdateDTO);
            if(updatesMedicationPlan.getMedicationPrescriptions()!=null)
                existingMedicationPlan.setMedicationPrescriptions(updatesMedicationPlan.getMedicationPrescriptions());
            if(updatesMedicationPlan.getName()!=null)
                existingMedicationPlan.setName(updatesMedicationPlan.getName());
            if(updatesMedicationPlan.getPeriodStart()!=null)
                existingMedicationPlan.setPeriodStart(updatesMedicationPlan.getPeriodStart());
            if(updatesMedicationPlan.getPeriodStop()!=null)
                existingMedicationPlan.setPeriodStop(updatesMedicationPlan.getPeriodStop());
            if(updatesMedicationPlan.getPatient()!=null)
                existingMedicationPlan.setPatient(updatesMedicationPlan.getPatient());

            MedicationPlan updatedMedication = medicationPlanRepository.save(existingMedicationPlan);

            return id;
        }else{
            return null;
        }
    }

}
