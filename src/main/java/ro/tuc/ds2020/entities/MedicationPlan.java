package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name="medicationPlan")
public class MedicationPlan  implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;

    @Column(name ="name", nullable = false)
    private String name;

    @Column(name = "periodStart", nullable = false)
    private String periodStart;

    @Column(name = "periodStop", nullable = false)
    private String periodStop;

    @OneToMany(mappedBy = "medicationPlan",cascade = CascadeType.REMOVE, fetch =FetchType.EAGER)
    private List<MedicationPrescription> medicationPrescriptions;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name="patient")//, insertable = false, updatable = false)
    private Patient patient;


    public MedicationPlan() {
    }

    public MedicationPlan(Long id, String name,String start, String stop, List<MedicationPrescription> medicationPrescriptions,Patient patient ) {
        this.id=id;
        this.name=name;
        this.periodStart=start;
        this.periodStop=stop;
        this.medicationPrescriptions=medicationPrescriptions;
        this.patient=patient;
    }

    public MedicationPlan(Long id, String name,String start, String stop,Patient patient) {
        this.id=id;
        this.name=name;
        this.periodStart=start;
        this.periodStop=stop;
        this.patient=patient;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String periodStart) {
        this.periodStart = periodStart;
    }

    public String getPeriodStop() {
        return periodStop;
    }

    public void setPeriodStop(String periodStop) { this.periodStop = periodStop; }

    public List<MedicationPrescription> getMedicationPrescriptions() {
        return medicationPrescriptions;
    }

    public void setMedicationPrescriptions(List<MedicationPrescription> medicationPrescriptions) {
        this.medicationPrescriptions = medicationPrescriptions;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
