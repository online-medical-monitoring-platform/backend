package ro.tuc.ds2020.dtos;


import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.dtos.PersonDTO;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class UserAppDTO extends RepresentationModel<UserAppDTO> implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String role;

    public UserAppDTO(Long id, String username, String password, String role) {
        this.id=id;
        this.username = username;
        this.password = password;
        this.role=role;
    }
    public UserAppDTO(){

    }

    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAppDTO patientDTO = (UserAppDTO) o;
        return Objects.equals(username, patientDTO.getUsername()) &&
                Objects.equals(password, patientDTO.getPassword()) &&
                Objects.equals(role, patientDTO.role) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username,password,role);
    }
}
