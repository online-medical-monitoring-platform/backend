package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.scheduling.support.SimpleTriggerContext;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Objects;

public class MedicationPrescriptionDTO extends RepresentationModel<MedicationPrescriptionDTO> implements Serializable {
    private Long id;
    private String intake;
    MedicationPlanDTO medicationPlan;
    MedicationDTO medication;
    private String administrated;

    public MedicationPrescriptionDTO() {
    }

    public MedicationPrescriptionDTO(Long id, String intake, MedicationDTO medication, MedicationPlanDTO medicationPlan) {
        this.id=id;
        this.intake = intake;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public MedicationPrescriptionDTO(Long id, String intake, MedicationDTO medication, MedicationPlanDTO medicationPlan,String administrated) {
        this.id=id;
        this.intake = intake;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
        this.administrated=administrated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medicaton) {
        this.medication = medicaton;
    }

    public MedicationPlanDTO getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlanDTO medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public String getAdministrated() {
        return administrated;
    }

    public void setAdministrated(String administrated) {this.administrated = administrated; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPrescriptionDTO medicationPrescriptionDTO = (MedicationPrescriptionDTO) o;
        return Objects.equals(intake, medicationPrescriptionDTO.intake) &&
                Objects.equals(medication, medicationPrescriptionDTO.medication) &&
                Objects.equals(medicationPlan, medicationPrescriptionDTO.medicationPlan)&&
                Objects.equals(administrated, medicationPrescriptionDTO.administrated) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(intake, medication,medicationPlan,administrated);
    }

}
