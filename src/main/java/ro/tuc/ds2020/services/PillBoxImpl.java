package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.UserAppDTO;
import ro.tuc.ds2020.dtos.builders.*;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.UserApp;
import ro.tuc.ds2020.repositories.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PillBoxImpl implements PillBoxInterface{

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);


   // @Autowired
    MedicationPlanRepository medicationPlanRepository;

   // @Autowired
    PatientRepository patientRepository;

   // @Autowired
    CaregiverRepository caregiverRepository;

   // @Autowired
    MedicationPrescriptionRepository medicationPrescriptionRepository;

   // @Autowired
    MedicationRepository medicationRepository;

   // @Autowired
    UserAppRepository userRepository;


    public PillBoxImpl(MedicationPlanRepository medicationPlanRepository,
                       PatientRepository patientRepository,
                       CaregiverRepository caregiverRepository,
                       MedicationPrescriptionRepository medicationPrescriptionRepository,
                       MedicationRepository medicationRepository,
                       UserAppRepository userRepository
    ){
        this.medicationPlanRepository=medicationPlanRepository;
        this.patientRepository=patientRepository;
        this.caregiverRepository=caregiverRepository;
        this.medicationPrescriptionRepository=medicationPrescriptionRepository;
        this.medicationRepository=medicationRepository;
        this.userRepository=userRepository;
    }



    //############ Medication Plan operations #############
    @Override
    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll();
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    @Override
    public MedicationPlanDTO findMedicationPlanById(Long id) {
        Optional<MedicationPlan> prosumerOptional = medicationPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication Plan with id {} was not found in db", id);
            //throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(prosumerOptional.get());
    }

    @Override
    public List<MedicationPlanDTO> findMedicationPlanByPatient(PatientDTO patient) {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findByPatient(PatientBuilder.toEntity(patient));
        if (!(medicationPlans.size()>0)) {
            LOGGER.error("Medication Plan with patient id {} was not found in db", patient.getId());
            //throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
            MedicationPlanDTO medPlan= new MedicationPlanDTO((long) 0,"notFound","notFound","notFound",patient);
            List<MedicationPlanDTO> list= new ArrayList<MedicationPlanDTO>();
            list.add(medPlan);
            return list;
        }
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<MedicationPrescriptionDTO> getMedicationPrescriptions(Long id) {
        List<MedicationPrescription> plans = medicationPlanRepository.getMedicationPrescriptions(id);
       /* System.out.println(a.size());
        System.out.println(a.get(0).toString());
        List<String> lista = Arrays.asList(a.get(0).toString().split(" "));
        System.out.println("lista este: " + lista.size());
        for(String s : lista){
            System.out.println(s);
        }*/
        System.out.println(plans.size());
        return plans.stream()
                .map(MedicationPrescriptionBuilder::toMedicationPrescriptionDTO)
                .collect(Collectors.toList());
    }





    //############ Patient operations #############

    @Override
    public List<PatientDTO> findPatients() {
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());    }

    @Override
    public PatientDTO findPatientById(Long id) {
        if(id==null)
            System.out.println("ID_UL E NULL");
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }


    @Override //for a patient specifying its id
    public List<MedicationPlanDTO> getMedicationPlan(Long id) {
        List<MedicationPlan> plans = patientRepository.getMedicationPlan(id);
        System.out.println(plans.size());
        return plans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }



    //############ Medication Prescription operations #############
    @Override
    public List<MedicationPrescriptionDTO> findMedicationPrescriptions() {
        List<MedicationPrescription> medicationPrescriptions = medicationPrescriptionRepository.findAll();
        return medicationPrescriptions.stream()
                .map(MedicationPrescriptionBuilder::toMedicationPrescriptionDTO)
                .collect(Collectors.toList());    }

    @Override
    public MedicationPrescriptionDTO findMedicationPrescriptionById(Long id) {
        Optional<MedicationPrescription> prosumerOptional = medicationPrescriptionRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication prescription with id {} was not found in db", id);
            //throw new ResourceNotFoundException(MedicationPrescription.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPrescriptionBuilder.toMedicationPrescriptionDTO(prosumerOptional.get());
    }

    @Override
    public Long update(Long id, MedicationPrescriptionDTO medicationPrescriptionUpdateDTO) {
        if (medicationPrescriptionRepository.findById(id).isPresent()){
            MedicationPrescription existingMedicationPrescription = medicationPrescriptionRepository.findById(id).get();
            if(medicationPrescriptionUpdateDTO.getMedication()==null) {//doar daca nu doreste sa schimbe medication i-l setez pe cel deja existent
                medicationPrescriptionUpdateDTO.setMedication(MedicationBuilder.toMedicationDTO(existingMedicationPrescription.getMedication()));
            }
            if(medicationPrescriptionUpdateDTO.getMedicationPlan()==null) {//doar daca nu doreste sa schimbe medication plan i-l setez pe cel deja existent
                medicationPrescriptionUpdateDTO.setMedicationPlan(MedicationPlanBuilder.toMedicationPlanDTO(existingMedicationPrescription.getMedicationPlan()));
            }
            //medicationPrescriptionUpdateDTO.setMedication(MedicationBuilder.toMedicationDTO(existingMedicationPrescription.getMedication()));
            //medicationPrescriptionUpdateDTO.setMedicationPlan(MedicationPlanBuilder.toMedicationPlanDTO(existingMedicationPrescription.getMedicationPlan()));
            MedicationPrescription updatesMedicationPrescription= MedicationPrescriptionBuilder.toEntity(medicationPrescriptionUpdateDTO);
            if(updatesMedicationPrescription.getIntake()!=null)
                existingMedicationPrescription.setIntake(updatesMedicationPrescription.getIntake());
            if(updatesMedicationPrescription.getMedication()!=null)
                existingMedicationPrescription.setMedication(updatesMedicationPrescription.getMedication());
            if(updatesMedicationPrescription.getMedicationPlan()!=null)
                existingMedicationPrescription.setMedicationPlan(updatesMedicationPrescription.getMedicationPlan());
            if(updatesMedicationPrescription.getAdministrated()!=null)
                existingMedicationPrescription.setAdministrated(updatesMedicationPrescription.getAdministrated());
            MedicationPrescription updatedMedication = medicationPrescriptionRepository.save(existingMedicationPrescription);

            return id;
        }else{
            return null;
        }
    }

    @Override
    public UserAppDTO findUserByUsernameAndPassword (String username, String password){
        Optional<UserApp> prosumerOptional = userRepository.findByUsernameAndPassword(username, password);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with username {} and password {} was not found in db", username, password);
            //throw new ResourceNotFoundException(UserApp.class.getSimpleName() + " username id: " + username);
            return new UserAppDTO((long) 0,"notFound","notFound","notFound");
        }
        return UserAppBuilder.toUserDTO(prosumerOptional.get());
    }

    @Override
    public String sayHelloWithHessian(String msg) {
        System.out.println("=============server side==============");
        System.out.println("msg : " + msg);
        return "Hello " + msg + " Response time :: " + new Date();
    }
}
