package ro.tuc.ds2020.entities;


import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;


@Entity
@Table(name="medicationPrescription")
public class MedicationPrescription  implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;

    @Column(name = "intake", nullable = false)
    private String intake;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicationPlan")
    private MedicationPlan medicationPlan;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "medication")
    private Medication medication;

    @Column(name = "administrated")
    private String administrated;

    public MedicationPrescription() {
    }

    public MedicationPrescription(Long id, String intake, Medication medication, MedicationPlan medicationPlan) {
        this.id=id;
        this.intake = intake;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }
    public MedicationPrescription(Long id, String intake, Medication medication, MedicationPlan medicationPlan, String administrated) {
        this.id=id;
        this.intake = intake;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
        this.administrated=administrated;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public String getAdministrated() {
        return administrated;
    }

    public void setAdministrated(String administrated) {this.administrated = administrated; }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
