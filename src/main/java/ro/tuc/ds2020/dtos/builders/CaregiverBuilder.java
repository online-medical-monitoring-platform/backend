package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
            return new CaregiverDTO(caregiver.getId(),caregiver.getUsername(), caregiver.getPassword(), caregiver.getName(), caregiver.getBirthdate(), caregiver.getGender(), caregiver.getAddress());
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getId(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress());

    }

}
