package ro.tuc.ds2020.Rmi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.services.*;
/*
@Configuration
public class RmiConfigurations {

    @Bean
    PillBoxInterface pillBox() {
        return new PillBoxImpl();
    }
    @Bean
    RmiServiceExporter exporter(PillBoxInterface implementation) {

        // Expose a service via RMI. Remote object URL is:
        // rmi://<HOST>:<PORT>/<SERVICE_NAME>
        // 1099 is the default port

        Class<PillBoxInterface> serviceInterface = PillBoxInterface.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(implementation);
        exporter.setServiceName(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }
    @Bean
    RemoteExporter registerRMIExporter() {

        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName(PillBoxInterface.class.getSimpleName());
        exporter.setServiceInterface(PillBoxInterface.class);
        exporter.setService(new PillBoxImpl());

        return exporter;
    }
}*/


