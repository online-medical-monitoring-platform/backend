package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;



import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {//implements PatientServiceInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository=caregiverRepository;
    }
    /*@Autowired
    PatientRepository patientRepository;

    @Autowired
    CaregiverRepository caregiverRepository;
    public PatientService(){}*/

    //@Override
    public List<PatientDTO> findPatients() {
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    //@Override
    public PatientDTO findPatientById(Long id) {
        if(id==null)
            System.out.println("ID_UL E NULL");
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }


    //@Override
    public Long insert(PatientDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    //@Override
    public Long delete(Long id){
        Patient patient = patientRepository.findById(id).get();
        patientRepository.delete(patient);
        LOGGER.debug("PATIENT WITH ID {} DELETED", id);
        return id;
    }

    //@Override
    public Long update(Long id, PatientDTO patientUpdateDTO) {

        if (patientRepository.findById(id).isPresent()){
            Patient existingPatient = patientRepository.findById(id).get();
            if(patientUpdateDTO.getCaregiver()==null) {//doar daca nu doreste sa schimbe caregiverul i-l setez pe cel la care era deja inregistrat
               if(existingPatient.getCaregiver()!=null)
                patientUpdateDTO.setCaregiver(CaregiverBuilder.toCaregiverDTO(existingPatient.getCaregiver()));
                System.out.println("Caregiverul dat nu e null");
            }

            Patient updatesPatient = PatientBuilder.toEntity(patientUpdateDTO);

            if(updatesPatient.getName()!=null)
            existingPatient.setName(updatesPatient.getName());
            if(updatesPatient.getBirthdate()!=null)
            existingPatient.setBirthdate(updatesPatient.getBirthdate());
            if(updatesPatient.getGender()!=null)
            existingPatient.setGender(updatesPatient.getGender());
            if(updatesPatient.getAddress()!=null)
            existingPatient.setAddress(updatesPatient.getAddress());
            if(updatesPatient.getCaregiver()!=null)
                existingPatient.setCaregiver(updatesPatient.getCaregiver());
            if(updatesPatient.getCaregiver()!=null)
                existingPatient.setCaregiver(updatesPatient.getCaregiver());
            if(updatesPatient.getMedicalRecord()!=null)
                existingPatient.setMedicalRecord(updatesPatient.getMedicalRecord());
            Patient updatedPatient = patientRepository.save(existingPatient);

            return id;
        }else{
            return null;
        }
    }

    //@Override
    public List<MedicationPlanDTO> getMedicationPlan(Long id) {
        List<MedicationPlan> plans = patientRepository.getMedicationPlan(id);
        System.out.println(plans.size());
        return plans.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }



}
