package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPrescriptionBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.repositories.MedicationPrescriptionRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;




import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPrescriptionService {//implements MedicationPrescriptionServiceInterface{
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPrescriptionService.class);
    private final MedicationPrescriptionRepository medicationPrescriptionRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPrescriptionService(MedicationPrescriptionRepository medicationPrescriptionRepository, MedicationPlanRepository medicationPlanRepository,MedicationRepository medicationRepository) {
        this.medicationPrescriptionRepository = medicationPrescriptionRepository;
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;

    }

    /*@Autowired
    MedicationPrescriptionRepository medicationPrescriptionRepository;

    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    MedicationPlanRepository medicationPlanRepository;

    public MedicationPrescriptionService(){}*/


    //@Override
    public List<MedicationPrescriptionDTO> findMedicationPrescriptions() {
        List<MedicationPrescription> medicationPrescriptions = medicationPrescriptionRepository.findAll();
        return medicationPrescriptions.stream()
                .map(MedicationPrescriptionBuilder::toMedicationPrescriptionDTO)
                .collect(Collectors.toList());
    }

    //@Override
    public MedicationPrescriptionDTO findMedicationPrescriptionById(Long id) {
        Optional<MedicationPrescription> prosumerOptional = medicationPrescriptionRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication prescription with id {} was not found in db", id);
            //throw new ResourceNotFoundException(MedicationPrescription.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPrescriptionBuilder.toMedicationPrescriptionDTO(prosumerOptional.get());
    }

    //@Override
    public Long insert(MedicationPrescriptionDTO medicationDTO) {
        MedicationPrescription medicationPrescription = MedicationPrescriptionBuilder.toEntity(medicationDTO);
        System.out.println("In Service"+medicationDTO.getMedicationPlan().getPeriodStart()+" "+medicationDTO.getMedicationPlan().getPeriodStop());
        medicationPrescription = medicationPrescriptionRepository.save(medicationPrescription);
        LOGGER.debug("medication prescription with id {} was inserted in db", medicationPrescription.getId());
        return medicationPrescription.getId();
    }

    //@Override
    public Long delete(Long id){
        MedicationPrescription medication = medicationPrescriptionRepository.findById(id).get();
        medicationPrescriptionRepository.delete(medication);
        LOGGER.debug("MEDICATION PRESCRIPTION WITH ID {} DELETED", id);

        return id;
    }

    //@Override
    public Long update(Long id, MedicationPrescriptionDTO medicationPrescriptionUpdateDTO) {
        if (medicationPrescriptionRepository.findById(id).isPresent()){
            MedicationPrescription existingMedicationPrescription = medicationPrescriptionRepository.findById(id).get();
            if(medicationPrescriptionUpdateDTO.getMedication()==null) {//doar daca nu doreste sa schimbe medication i-l setez pe cel deja existent
                medicationPrescriptionUpdateDTO.setMedication(MedicationBuilder.toMedicationDTO(existingMedicationPrescription.getMedication()));
            }
            if(medicationPrescriptionUpdateDTO.getMedicationPlan()==null) {//doar daca nu doreste sa schimbe medication plan i-l setez pe cel deja existent
                medicationPrescriptionUpdateDTO.setMedicationPlan(MedicationPlanBuilder.toMedicationPlanDTO(existingMedicationPrescription.getMedicationPlan()));
            }
            //medicationPrescriptionUpdateDTO.setMedication(MedicationBuilder.toMedicationDTO(existingMedicationPrescription.getMedication()));
            //medicationPrescriptionUpdateDTO.setMedicationPlan(MedicationPlanBuilder.toMedicationPlanDTO(existingMedicationPrescription.getMedicationPlan()));
            MedicationPrescription updatesMedicationPrescription= MedicationPrescriptionBuilder.toEntity(medicationPrescriptionUpdateDTO);
            if(updatesMedicationPrescription.getIntake()!=null)
                existingMedicationPrescription.setIntake(updatesMedicationPrescription.getIntake());
            if(updatesMedicationPrescription.getMedication()!=null)
                existingMedicationPrescription.setMedication(updatesMedicationPrescription.getMedication());
            if(updatesMedicationPrescription.getMedicationPlan()!=null)
                existingMedicationPrescription.setMedicationPlan(updatesMedicationPrescription.getMedicationPlan());
            if(updatesMedicationPrescription.getAdministrated()!=null)
                existingMedicationPrescription.setAdministrated(updatesMedicationPrescription.getAdministrated());
            MedicationPrescription updatedMedication = medicationPrescriptionRepository.save(existingMedicationPrescription);

            return id;
        }else{
            return null;
        }
    }



}
