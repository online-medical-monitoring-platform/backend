package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.Optional;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {
    List<MedicationPlan> findByPatient(Patient patient);

    /**
     * Example: JPA generate Query by Field
     */
   // Optional<MedicationPlan> findById(Long id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "select m from MedicationPrescription m " +
            "join MedicationPlan mp on m.medicationPlan = mp " +
            "where mp.id=:id")
    List<MedicationPrescription> getMedicationPrescriptions(@Param("id") Long id);



}
