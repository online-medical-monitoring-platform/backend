package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.MedicationPrescription;

import java.util.List;
import java.util.Optional;

public interface MedicationPrescriptionRepository extends JpaRepository<MedicationPrescription, Long> {

    /**
     * Example: JPA generate Query by Field
     */
   // List<MedicationPrescription> findById(Long id);
    //Optional<MedicationPrescription> findById( Long id);

    /**
     * Example: Write Custom Query
     */
   /* @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.name = :name ")
    Optional<MedicationPrescription> findPatientsByCaregiver(@Param("name") String name);
*/

}
