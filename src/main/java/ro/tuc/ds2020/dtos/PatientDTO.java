package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.scheduling.support.SimpleTriggerContext;
import ro.tuc.ds2020.entities.Caregiver;

import java.io.Serializable;
import java.util.Objects;

public class PatientDTO extends RepresentationModel<PatientDTO> implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private String medicalRecord;
    private CaregiverDTO caregiver;
    private String role;

    public PatientDTO() {
    }

    public PatientDTO(Long id,String username,String password, String name, String birthdate, String gender, String address, CaregiverDTO caregiver,String medicalRecord) {
        this.id=id;
        this.username=username;
        this.password=password;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.caregiver=caregiver;
        this.medicalRecord=medicalRecord;
        this.role="patient";
    }
    public PatientDTO(Long id,String username,String password, String name, String birthdate, String gender, String address,String medicalRecord) {
        this.id=id;
        this.username=username;
        this.password=password;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord=medicalRecord;
        this.role="patient";
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public CaregiverDTO getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(CaregiverDTO caregiver) {
        this.caregiver = caregiver;
    }


    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(username, patientDTO.getUsername()) &&
                Objects.equals(password, patientDTO.getPassword()) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(birthdate, patientDTO.birthdate) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address) &&
                Objects.equals(caregiver, patientDTO.caregiver) &&
                Objects.equals(medicalRecord, patientDTO.medicalRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username,password,name, birthdate, gender,address,caregiver,medicalRecord);
    }

    public String toStringCaregiver(){
        return "Caregiver: "+ caregiver.getId();
    }
}
