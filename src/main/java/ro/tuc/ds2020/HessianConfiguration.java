package ro.tuc.ds2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.repositories.*;
import ro.tuc.ds2020.services.PillBoxImpl;
import ro.tuc.ds2020.services.PillBoxInterface;


@Configuration
public class HessianConfiguration {

     @Autowired
    MedicationPlanRepository medicationPlanRepository;

     @Autowired
    PatientRepository patientRepository;

    @Autowired
    CaregiverRepository caregiverRepository;

     @Autowired
    MedicationPrescriptionRepository medicationPrescriptionRepository;

     @Autowired
    MedicationRepository medicationRepository;

     @Autowired
    UserAppRepository userRepository;


    @Bean(name = "/hellohessian")
    RemoteExporter sayHelloServiceHessian() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new PillBoxImpl( medicationPlanRepository,
                 patientRepository,
                 caregiverRepository,
                 medicationPrescriptionRepository,
                 medicationRepository,
                 userRepository));
        exporter.setServiceInterface(PillBoxInterface.class);
        return exporter;
    }

}
