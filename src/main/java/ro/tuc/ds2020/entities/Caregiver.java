package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
//@PrimaryKeyJoinColumn(name = "id")
@Table(name="caregiver")
public class Caregiver extends UserApp{

    /*@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;*/

    @Column(name = "name")
    private String name;

    @Column(name = "birthdate")
    private String birthdate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @OneToMany(/*cascade=CascadeType.ALL ,*/ mappedBy = "caregiver", fetch =FetchType.EAGER)//, orphanRemoval = true)
    private List<Patient> patients;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }




    public Caregiver(Long id,String username,String password, String name, String birthdate, String gender, String address, List<Patient> patients ) {
        this.setId(id);
        this.setUsername(username);
        this.setPassword(password);
        this.name=name;
        this.gender=gender;
        this.birthdate=birthdate;
        this.address=address;
        this.patients=patients;
        this.setRole("caregiver");
    }

    public Caregiver() {

    }

    public Caregiver(Long id,String username,String password, String name, String birthdate, String gender, String address) {
        this.setId(id);
        this.setUsername(username);
        this.setPassword(password);
        this.name=name;
        this.gender=gender;
        this.birthdate=birthdate;
        this.address=address;
        this.setRole("caregiver");
    }

}
