package ro.tuc.ds2020.entities;

        import org.hibernate.annotations.GenericGenerator;
        import org.hibernate.annotations.Type;

        import javax.persistence.*;
        import javax.persistence.Entity;
        import java.io.Serializable;
        import java.util.List;


@Entity
@Table(name="medication")
public class Medication  implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sideEffects", nullable = true)
    private String sideEffects;

    @Column(name = "dosage", nullable = false)
    private String dosage;

    @OneToMany(mappedBy = "medication",cascade = CascadeType.REMOVE,fetch =FetchType.EAGER)
    private List<MedicationPrescription> medicationPrescriptions;

    public Medication() {
    }

    public Medication(Long id,String name, String sideEffects, String dosage, List<MedicationPrescription> medicationPrescriptions) {
        this.id=id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.medicationPrescriptions=medicationPrescriptions;
    }

    public Medication(Long id,String name, String sideEffects, String dosage) {
        this.id=id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public List<MedicationPrescription> getMedicationPrescriptions() {
        return medicationPrescriptions;
    }

    public void setMedicationPrescriptions(List<MedicationPrescription> medicationPrescriptions) {
        this.medicationPrescriptions = medicationPrescriptions;
    }
}
