package ro.tuc.ds2020.dtos;

        import org.springframework.hateoas.RepresentationModel;
        import org.springframework.scheduling.support.SimpleTriggerContext;

        import javax.persistence.Column;
        import java.io.Serializable;
        import java.util.Objects;

public class MedicationDTO extends RepresentationModel<MedicationDTO> implements Serializable {
    private Long id;
    private String name;
    private String sideEffects;
    private String dosage;

    public MedicationDTO() {
    }

    public MedicationDTO(Long id,String name, String sideEffects, String dosage) {
        this.id=id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getSideEffects() { return sideEffects; }

    public void setSideEffects(String sideEffects) { this.sideEffects = sideEffects; }

    public String getDosage() { return dosage; }

    public void setDosage(String dosage) { this.dosage = dosage; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO madicationDTO = (MedicationDTO) o;
        return Objects.equals(name, madicationDTO.name) &&
                Objects.equals(sideEffects, madicationDTO.sideEffects) &&
                Objects.equals(dosage, madicationDTO.dosage) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }

    public String toStringMedication(){
        return "Medication: "+ name;
    }
}
