package ro.tuc.ds2020.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name="activity")
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique = true, nullable = false)
    private Long id;

    @Column(name = "activity", nullable = false)
    private String activity;

    @Column(name = "startActivity", nullable = false)
    private String start;

    @Column(name = "endActivity", nullable = false)
    private String end;

    @Column(name = "patientId", nullable = false)
    private Long patientId;


    public Activity() {

    }

    public Activity(Long id,Long patientId, String activity, String start, String end) {
        this.id=id;
        this.patientId = patientId;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }
    public Activity(Long patientId, String activity, String start, String end) {
        this.patientId = patientId;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }


    public Long getId() {
        return id;
    }

    public String getActivity() {
        return activity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getStart() {
        return start;
    }
    public String getEnd() {
        return end;
    }
    public Long getPatientId() {
        return patientId;
    }
}
