package ro.tuc.ds2020.services;

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        //import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
        import ro.tuc.ds2020.dtos.UserAppDTO;
        import ro.tuc.ds2020.dtos.builders.UserAppBuilder;
        import ro.tuc.ds2020.entities.UserApp;
        import ro.tuc.ds2020.repositories.UserAppRepository;

        import java.util.List;
        import java.util.Optional;
        import java.util.stream.Collectors;

@Service
public class UserAppService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAppService.class);
    //private final PersonRepository personRepository;
    private final UserAppRepository userRepository;

    @Autowired
    public UserAppService(UserAppRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserAppDTO> findUsers() {
        List<UserApp> personList = userRepository.findAll();
        return personList.stream()
                .map(UserAppBuilder::toUserDTO)
                .collect(Collectors.toList());
    }
    public UserAppDTO findUserByUsernameAndPassword (String username, String password){
        Optional<UserApp> prosumerOptional = userRepository.findByUsernameAndPassword(username, password);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with username {} and password {} was not found in db", username, password);
            //throw new ResourceNotFoundException(UserApp.class.getSimpleName() + " username id: " + username);
            return new UserAppDTO((long) 0,"notFound","notFound","notFound");
        }
        return UserAppBuilder.toUserDTO(prosumerOptional.get());
    }

    public UserAppDTO findUserAppById(Long id) {
        Optional<UserApp> prosumerOptional = userRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            //throw new ResourceNotFoundException(UserApp.class.getSimpleName() + " with id: " + id);
        }
        return UserAppBuilder.toUserDTO(prosumerOptional.get());
    }

    public Long insert(UserAppDTO personDTO) {
        UserApp person = UserAppBuilder.toEntity(personDTO);
        person = userRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public Long delete(Long id){
        UserApp person = userRepository.findById(id).get();
        userRepository.delete(person);
        LOGGER.debug("PERSON WITH ID {} DELETED", id);

        return id;
    }
   /* public Long deleteByName(String name){

        User person = userRepository.findByName(name).get(0);
        userRepository.delete(person);
        LOGGER.debug("PERSON WITH NAME {} DELETED", name);

        return person.getId();
    }*/

    public Long update(Long id, UserAppDTO personUpdateDTO) {
        UserApp updatesPerson = UserAppBuilder.toEntity(personUpdateDTO);
        if (userRepository.findById(id).isPresent()){
            UserApp existingPerson = userRepository.findById(id).get();

            if(updatesPerson.getPassword()!=null)
                existingPerson.setPassword(updatesPerson.getPassword());
            if(updatesPerson.getUsername()!=null)
                existingPerson.setUsername(updatesPerson.getUsername());

            UserApp updatedPerson = userRepository.save(existingPerson);

            return id;
        }else{
            return null;
        }
    }



}
