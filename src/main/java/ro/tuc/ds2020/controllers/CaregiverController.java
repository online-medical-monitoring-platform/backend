package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;


    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        /*for (CaregiverDTO dto : dtos) {
            Link caregiverLink = linkTo(methodOn(CaregiverController.class)
                    .getCaregiver(dto.getId())).withRel("caregiverDetails");
            dto.add(caregiverLink);
        }*/
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
       // caregiverDTO.setUserApp(userAppService.findUserById(caregiverDTO.getUserApp().getId()));
        Long caregiverID = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") Long caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        System.out.println();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/patient")
    public ResponseEntity<List<PatientDTO>> getPatients(@PathVariable("id") Long caregiverId) {
        List<PatientDTO> patients= caregiverService.getPatients(caregiverId);
        if(patients==null || patients.size() ==0)
            patients= new ArrayList<PatientDTO>();

        return new ResponseEntity<>(patients, HttpStatus.OK);
    }


     @DeleteMapping(value = "/{id}")
     public ResponseEntity<Long> deleteCaregiver(@PathVariable("id") Long id){
         Long caregiverID = caregiverService.delete(id);
         return new ResponseEntity<>(caregiverID, HttpStatus.OK);
     }


    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody CaregiverDTO caregiverUpdateDTO) {
        Long caregiverID = caregiverService.update(id, caregiverUpdateDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.ACCEPTED);
    }
}
