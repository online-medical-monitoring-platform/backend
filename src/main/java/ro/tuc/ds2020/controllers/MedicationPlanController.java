package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;
    private final PatientService patientService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService, PatientService patientService) {
        this.medicationPlanService = medicationPlanService;
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlans();
       /* for (MedicationDTO dto : dtos) {
            Link personLink = linkTo(methodOn(MedicationController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }*/
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/medicationPrescription")
    public ResponseEntity<List<MedicationPrescriptionDTO>> getMedicationPrescriptions(@PathVariable("id") Long medicationaPlanId) {
        List<MedicationPrescriptionDTO> plans= medicationPlanService.getMedicationPrescriptions(medicationaPlanId);
        if(plans==null || plans.size() ==0)
            plans= new ArrayList<MedicationPrescriptionDTO>();

        return new ResponseEntity<>(plans, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        PatientDTO patient= patientService.findPatientById(medicationPlanDTO.getPatient().getId());
        medicationPlanDTO.setPatient(patient);
        Long medicationPlanID = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationPlanDTO> getMedicationPlan(@PathVariable("id") Long medicationPlanID) {
        MedicationPlanDTO dto = medicationPlanService.findMedicationPlanById(medicationPlanID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/patient/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlansByPatient(@PathVariable("id") Long id) {
        PatientDTO patient=patientService.findPatientById(id);
        System.out.println(patient.getName());
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlanByPatient(patient);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deleteMedicationPlan(@PathVariable("id") Long id){
        Long medicationPlanID = medicationPlanService.delete(id);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.OK);
    }
   /* @DeleteMapping(value = "/{name}")
    public ResponseEntity<MedicationDTO> deleteMedicationByName(@PathVariable("name") String name){
        Long medicationID = medicationService.deleteByName(name);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }*/

    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody MedicationPlanDTO medicationPlanUpdateDTO) {
        Long medicationPlanID = medicationPlanService.update(id, medicationPlanUpdateDTO);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }
}
