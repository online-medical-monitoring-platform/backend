package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Patient> findByName(String name);
    List<Patient> findByCaregiver(Caregiver caregiver);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.caregiver = :caregiver ")
    List<Patient> findPatientsByCaregiverID(@Param("caregiver") Caregiver caregiver);

  /*  @Query(value = "select mp.id ,mp.period_start,mp.period_stop, m.name,m.dosage, m.side_effects,mmp.intake from Patient p " +
            "join medication_plan mp on mp.patient = p.id " +
            "join medication_prescription mmp on mmp.medication_plan = mp.id " +
            "join medication m on m.id = mmp.medication " +
            "where p.id=:id",nativeQuery = true)
    List<Object> getMedicationPlan(@Param("id") Long id);*/
  @Query(value = "select mp from MedicationPlan mp " +
          "join Patient p on mp.patient = p " +
          "where p.id=:id")
  List<MedicationPlan> getMedicationPlan(@Param("id") Long id);

}
