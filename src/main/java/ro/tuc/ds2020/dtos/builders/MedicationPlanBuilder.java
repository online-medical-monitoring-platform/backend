package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getName(),medicationPlan.getPeriodStart(),medicationPlan.getPeriodStop(),PatientBuilder.toPatientDTO(medicationPlan.getPatient()));

    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan( medicationPlanDTO.getId(),
                medicationPlanDTO.getName(),
                medicationPlanDTO.getPeriodStart(),
                medicationPlanDTO.getPeriodStop(),
                PatientBuilder.toEntity(medicationPlanDTO.getPatient()));
    }

}
