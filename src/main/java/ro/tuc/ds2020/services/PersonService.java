package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDetailsDTO findPersonById(UUID id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
           // throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(PersonDetailsDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public UUID delete(UUID id){
        Person person = personRepository.findById(id).get();
        personRepository.delete(person);
        LOGGER.debug("PERSON WITH ID {} DELETED", id);

        return id;
    }
    public UUID deleteByName(String name){

        Person person = personRepository.findByName(name).get(0);
        personRepository.delete(person);
        LOGGER.debug("PERSON WITH NAME {} DELETED", name);

        return person.getId();
    }

    public UUID update(UUID id, PersonDetailsDTO personUpdateDTO) {
        Person updatesPerson = PersonBuilder.toEntity(personUpdateDTO);
        if (personRepository.findById(id).isPresent()){
            Person existingPerson = personRepository.findById(id).get();

            if(updatesPerson.getAddress()!=null)
            existingPerson.setAddress(updatesPerson.getAddress());
            existingPerson.setAge(updatesPerson.getAge());
            if(updatesPerson.getName()!=null)
            existingPerson.setName(updatesPerson.getName());

            Person updatedPerson = personRepository.save(existingPerson);

            return id;
        }else{
            return null;
        }
    }



}
