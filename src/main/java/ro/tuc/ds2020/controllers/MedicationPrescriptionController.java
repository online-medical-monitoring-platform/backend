package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.MedicationPrescription;
import ro.tuc.ds2020.services.MedicationPrescriptionService;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.MedicationService;



import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPrescription")
public class MedicationPrescriptionController {

    private final MedicationPrescriptionService medicationPrescriptionService;
    private final MedicationService medicationService;
    private final MedicationPlanService medicationPlanService;



    @Autowired
    public MedicationPrescriptionController(MedicationPrescriptionService medicationPrescriptionService,MedicationService medicationService, MedicationPlanService medicationPlanService) {
        this.medicationPrescriptionService = medicationPrescriptionService;
        this.medicationPlanService=medicationPlanService;
        this.medicationService=medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPrescriptionDTO>> getMedicationPrescriptions() {
        List<MedicationPrescriptionDTO> dtos = medicationPrescriptionService.findMedicationPrescriptions();
       /* for (MedicationDTO dto : dtos) {
            Link personLink = linkTo(methodOn(MedicationController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }*/
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertMedicationPrescription(@Valid @RequestBody MedicationPrescriptionDTO medicationPrescriptionDTO) {
        MedicationPlanDTO medicationPlan= medicationPlanService.findMedicationPlanById(medicationPrescriptionDTO.getMedicationPlan().getId());
        medicationPrescriptionDTO.setMedicationPlan(medicationPlan);

        MedicationDTO medication=medicationService.findMedicationById(medicationPrescriptionDTO.getMedication().getId());
        medicationPrescriptionDTO.setMedication(medication);

        System.out.println("Medication plan"+ medicationPlan.getPeriodStart()+medicationPlan.getPeriodStop());
        Long medicationPrescriptionID = medicationPrescriptionService.insert(medicationPrescriptionDTO);
        return new ResponseEntity<>(medicationPrescriptionID, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationPrescriptionDTO> getMedicationPrescription(@PathVariable("id") Long medicationPrescriptionID) {
        MedicationPrescriptionDTO dto = medicationPrescriptionService.findMedicationPrescriptionById(medicationPrescriptionID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deleteMedicationPrescription(@PathVariable("id") Long id){
        Long medicationPrescriptionID = medicationPrescriptionService.delete(id);
        return new ResponseEntity<>(medicationPrescriptionID, HttpStatus.OK);
    }


    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody MedicationPrescriptionDTO medicationPrescriptionUpdateDTO) {
        Long medicationPrescriptionID = medicationPrescriptionService.update(id, medicationPrescriptionUpdateDTO);
        return new ResponseEntity<>(medicationPrescriptionID, HttpStatus.CREATED);
    }
}
