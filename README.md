Project Setup (after clone)

Clone the project to your computer
Import the project in InteliJ (or your IDE)
Create a database schema "sd-db" in PostgreSQL
Comment lines 4-8
Decommens lines 10-13 and chane name and password according to your database
Change the line 24 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = create
Run the application
Check is the person table was created
Change the line 24 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = update
