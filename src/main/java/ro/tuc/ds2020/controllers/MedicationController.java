package ro.tuc.ds2020.controllers;


        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.hateoas.Link;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.*;
        import ro.tuc.ds2020.dtos.MedicationDTO;
        import ro.tuc.ds2020.entities.Medication;
        import ro.tuc.ds2020.services.MedicationService;

        import javax.validation.Valid;
        import java.util.List;
        import java.util.UUID;

        import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
        import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
       /* for (MedicationDTO dto : dtos) {
            Link personLink = linkTo(methodOn(MedicationController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }*/
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        Long medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") Long medicationID) {
        MedicationDTO dto = medicationService.findMedicationById(medicationID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deleteMedication(@PathVariable("id") Long id){
        Long medicationID = medicationService.delete(id);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }
   /* @DeleteMapping(value = "/{name}")
    public ResponseEntity<MedicationDTO> deleteMedicationByName(@PathVariable("name") String name){
        Long medicationID = medicationService.deleteByName(name);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }*/

    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody MedicationDTO medicationUpdateDTO) {
        Long medicationID = medicationService.update(id, medicationUpdateDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.ACCEPTED);
    }
}
