package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final CaregiverService caregiverService;

    @Autowired
    public PatientController(PatientService patientService, CaregiverService caregiverService) {
        this.patientService = patientService;
        this.caregiverService = caregiverService;
    }



    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        /*for (PatientDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatient(dto.getId())).withRel("patientDetails");
            dto.add(patientLink);
        }*/
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertPatient(@Valid @RequestBody PatientDTO patientDTO) {
        if(patientDTO.getCaregiver()==null)
            System.out.println("e null caregiverul pacientului introdus");
       else {
            CaregiverDTO caregiver = caregiverService.findCaregiverById(patientDTO.getCaregiver().getId());
            patientDTO.setCaregiver(caregiver);
        }

        Long patientID = patientService.insert(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") Long patientId) {
        PatientDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/caregiver")
    public ResponseEntity<CaregiverDTO> getPatientCaregiver(@PathVariable("id") Long patientId) {
        PatientDTO patientDTO = patientService.findPatientById(patientId);
        if(patientDTO!=null) {
            CaregiverDTO caregiverDTO=null;
            if (patientDTO.getCaregiver() == null)
                System.out.println("pacientu nu are caregiver");
            else {
                caregiverDTO = caregiverService.findCaregiverById(patientDTO.getCaregiver().getId());
            }
            return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{id}/medicationPlan")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlan(@PathVariable("id") Long patientId) {
        List<MedicationPlanDTO> plans= patientService.getMedicationPlan(patientId);
        if(plans==null || plans.size() ==0)
            plans= new ArrayList<MedicationPlanDTO>();

        return new ResponseEntity<>(plans, HttpStatus.OK);
    }

    @GetMapping(value = "/{username}/{password}")
    public ResponseEntity<PatientDTO> getPatientByUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        List<PatientDTO> dtos = patientService.findPatients();
        PatientDTO patient=null;
        for (PatientDTO dto : dtos) {
            if(dto.getUsername().equals(username) && dto.getPassword().equals(password)) {
                patient = dto;
                break;
            }
        }
        System.out.println("Username si parola :"+username+ "  "+ password);
        System.out.println("Pacient gasit"+ patient);
        if(patient!=null)
            return new ResponseEntity<>(patient, HttpStatus.OK);
        else return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }



      @DeleteMapping(value = "/{id}")
      public ResponseEntity<Long> deletePatient(@PathVariable("id") Long id){
          Long patientID = patientService.delete(id);
          return new ResponseEntity<>(patientID, HttpStatus.OK);
      }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody PatientDTO patientUpdateDTO) {
        Long patientID = patientService.update(id, patientUpdateDTO);
        return new ResponseEntity<>(patientID, HttpStatus.ACCEPTED);
    }
}
