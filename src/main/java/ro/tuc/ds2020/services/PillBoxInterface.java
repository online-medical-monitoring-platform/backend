package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.UserAppDTO;

import java.util.List;

public interface PillBoxInterface {
    public List<MedicationPlanDTO> findMedicationPlans();
    public MedicationPlanDTO findMedicationPlanById(Long id);
    public List<MedicationPlanDTO> findMedicationPlanByPatient(PatientDTO patient);
    public List<MedicationPrescriptionDTO> getMedicationPrescriptions(Long id);

    public List<PatientDTO> findPatients();
    public PatientDTO findPatientById(Long id);
    public List<MedicationPlanDTO> getMedicationPlan(Long id);

    public List<MedicationPrescriptionDTO> findMedicationPrescriptions();
    public MedicationPrescriptionDTO findMedicationPrescriptionById(Long id);
    public Long update(Long id, MedicationPrescriptionDTO medicationPrescriptionUpdateDTO);

    public UserAppDTO findUserByUsernameAndPassword (String username, String password);

    public String sayHelloWithHessian(String msg);
}
