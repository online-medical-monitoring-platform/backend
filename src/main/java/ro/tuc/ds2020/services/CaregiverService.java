
package ro.tuc.ds2020.services;

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import org.springframework.transaction.annotation.Transactional;
        //import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
        import ro.tuc.ds2020.dtos.CaregiverDTO;
        import ro.tuc.ds2020.dtos.PatientDTO;
        import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
        import ro.tuc.ds2020.dtos.builders.PatientBuilder;
        import ro.tuc.ds2020.entities.Caregiver;
        import ro.tuc.ds2020.entities.Patient;
        import ro.tuc.ds2020.repositories.CaregiverRepository;
        import ro.tuc.ds2020.repositories.PatientRepository;

        import java.util.List;
        import java.util.Optional;
        import java.util.stream.Collectors;

@Service
public class CaregiverService{
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(Long id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public Long insert(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    @Transactional
    public Long delete(Long id){
        Caregiver caregiver = caregiverRepository.findById(id).get();
        List<Patient> patients=patientRepository.findByCaregiver(caregiver);
        System.out.println(patients.size());
        for(Patient p:patients)
            p.setCaregiver(null);
        caregiverRepository.delete(caregiver);
        LOGGER.debug("CAREGIVER WITH ID {} DELETED", id);

        return id;
    }
     public Long deleteByName(String name){

        Caregiver caregiver = caregiverRepository.findByName(name).get(0);
        caregiverRepository.delete(caregiver);
        LOGGER.debug("CAREGIVER WITH NAME {} DELETED", name);
        return caregiver.getId();
    }

     public Long update(Long id, CaregiverDTO caregiverUpdateDTO) {
        Caregiver updatesCaregiver = CaregiverBuilder.toEntity(caregiverUpdateDTO);
        if (caregiverRepository.findById(id).isPresent()){
            Caregiver existingCaregiver = caregiverRepository.findById(id).get();
            if(updatesCaregiver.getName()!=null)
                existingCaregiver.setName(updatesCaregiver.getName());
            if(updatesCaregiver.getBirthdate()!=null)
                existingCaregiver.setBirthdate(updatesCaregiver.getBirthdate());
            if(updatesCaregiver.getGender()!=null)
                existingCaregiver.setGender(updatesCaregiver.getGender());
            if(updatesCaregiver.getAddress()!=null)
                existingCaregiver.setAddress(updatesCaregiver.getAddress());
            if(updatesCaregiver.getPatients()!=null)
                existingCaregiver.setPatients(updatesCaregiver.getPatients());
            Caregiver updatedCaregiver = caregiverRepository.save(existingCaregiver);

            return id;
        }else{
            return null;
        }
    }

    public List<PatientDTO> getPatients(Long id) {
        List<Patient> plans = caregiverRepository.getPatients(id);
        System.out.println(plans.size());
        return plans.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

}
