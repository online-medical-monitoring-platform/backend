package ro.tuc.ds2020.services;

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        //import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
        import ro.tuc.ds2020.dtos.MedicationDTO;
        import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
        import ro.tuc.ds2020.entities.Medication;
        import ro.tuc.ds2020.repositories.MedicationRepository;


        import java.util.List;
        import java.util.Optional;
        import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;


    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(Long id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
    }

    public Long insert(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public Long delete(Long id){
        Medication medication = medicationRepository.findById(id).get();
        medicationRepository.delete(medication);
        LOGGER.debug("MEDICATION WITH ID {} DELETED", id);

        return id;
    }
    public Long deleteByName(String name){
        Medication medication = medicationRepository.findByName(name).get(0);
        medicationRepository.delete(medication);
        LOGGER.debug("MEDICATION WITH NAME {} DELETED", name);

        return medication.getId();
    }

    public Long update(Long id, MedicationDTO medicationUpdateDTO) {
        Medication updatesMedication= MedicationBuilder.toEntity(medicationUpdateDTO);
        if (medicationRepository.findById(id).isPresent()){
            Medication existingMedication = medicationRepository.findById(id).get();

            if(updatesMedication.getName()!=null)
                existingMedication.setName(updatesMedication.getName());
            if(updatesMedication.getSideEffects()!=null)
                existingMedication.setSideEffects(updatesMedication.getSideEffects());
            if(updatesMedication.getDosage()!=null)
                existingMedication.setDosage(updatesMedication.getDosage());
            if(updatesMedication.getMedicationPrescriptions()!=null)
                existingMedication.setMedicationPrescriptions(updatesMedication.getMedicationPrescriptions());
            Medication updatedMedication = medicationRepository.save(existingMedication);

            return id;
        }else{
            return null;
        }
    }



}
