package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.UserApp;

import java.util.Optional;

public interface UserAppRepository extends JpaRepository<UserApp, Long> {
    Optional<UserApp> findByUsernameAndPassword(String username, String password);

    /**
     * Example: JPA generate Query by Field
     */
    // List<User> findById(Long id);
}
